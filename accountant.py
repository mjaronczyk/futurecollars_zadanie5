





class Accountant:


    def __init__(self):
        self.balance = 0
        self.history = []
        self.assortment = {}

        self.load_data()
        self.menu()
        self.save_show()




        
    def load_data(self):
        with open("balance.txt") as balance:
            self.balance = int(balance.read())
        balance.close()
        

        with open("history.txt") as history:
            for element in history.readlines():
                self.history.append(element)
        try:
            self.history = [line.strip() for line in self.history]
        except:
            print("Something Wrong at line 35")


        history.close()
        print(f'self.history: {self.history}')
        
        
        with open("assortment.txt") as assortment:
            for element in assortment.readlines():
                if len(element.split()) == 2:
                    self.assortment[element.split()[0]] = int(element.split()[1])
                else:
                    key = ""
                    for index in range(len(element.split())-1):
                        key = key + str(element.split()[index]) + " "
                    
                    self.assortment[key[:-1]] = int(element.split()[-1])
        
        assortment.close()


        

    def save_show(self):
        print(f'\nBalance: {self.balance}\n')
        balance = open("balance.txt", "w")
        balance.write(str(self.balance))
        balance.close()


        print(f'\nActions: {len(self.history)}')
        history = open("history.txt", "w")
        for line in self.history:
            print(line)
            history.write(f'{line}\n')
        history.close()


        print('\nAssortment:')
        assortment = open("assortment.txt", 'w')
        for line in self.assortment:
            print(f'Id: {line}: Qty: {self.assortment[line]}')
            a = f'{line} {str(self.assortment[line])}\n'
            assortment.write(a)
        assortment.close()





        



    def menu(self):
        while True:
            print()
            print('Choose one of the following options: \nsaldo / kupno / sprzedaz / wyjscie ')
            print()
            action = input("\nPress the action type: ")
            

            if action == 'saldo':
                price = int(input("Amount: "))
                comment = input("Comment: ")
                self.history.append((f'Saldo, {price}, {comment}'))
                self.balance += price
                print(f'\nDodano:\nSaldo, {price}, {comment}')



            elif action == 'kupno':
                id = input("Product ID: ")
                if id[-1] == " ": id = id[:-1]
                price = int(input("Price: "))
                quantity = int(input("Quantity: "))
                if (price * quantity < self.balance) and (quantity > 0 and price > 0):
                    self.balance -= price * quantity
                    self.history.append((f'Bought, id: {id}, price: {price}, qty: {quantity}'))
                    print(f'Bought, id: {id}, price: {price}, qty: {quantity}')
                    try:
                        self.assortment[id] += quantity
                    except:
                        self.assortment[id] = quantity
                else:
                    print("Error occured. Try again!")
                print(self.assortment)
            



            elif action == 'sprzedaz':
                id = input("Product ID: ")
                price = int(input("Price: "))
                quantity = int(input("Quantity: "))
                try:
                    if (price > 0 and quantity > 0) and (self.assortment.get(id) >= quantity):
                        self.balance += price * quantity
                        self.assortment[id] -= quantity
                        self.history.append((f'Sold, id: {id}, price: {price}, qty: {quantity}'))
                        print(f'Sold, id: {id}, price: {price}, qty: {quantity}')
                    else:
                        print("Error occured. Make sure qty and price are correct")
                except:
                    print("Make sure the product is in stock ")

            elif action == "wyjscie":
                break
            
            else:
                print("Incorect Type. Try again")
    




ob1 = Accountant()
